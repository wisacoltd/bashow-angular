module.exports = ( () => {
	angular.module('rsea-angular',['ngSanitize'])
			.directive('autoResize' ,['$timeout',($timeout)=> {
				return {
					restrict : 'A',
					link : (scope,element,attrs,controller) => {
						var min = attrs.autoResize ? attrs.autoResize : 'auto';
						var widthOffset = attrs.widthOffset ? attrs.widthOffset : 0;
						var heightOffset = attrs.heightOffset ? attrs.heightOffset : 0;
						element.css({ 'height': min , 'overflow-y': 'hidden' });
		                $timeout(function () {
                    		element.css('height', element[0].scrollHeight + 'px');
		                }, 100);
						$('textarea#temp-textarea').remove();
						var $temp = $(element).clone();
						$temp.removeAttr('id','');
						$temp.attr('id','temp-textarea');
						$temp.css( { width: 'calc(100% - ' +  widthOffset+ 'px)', position : 'absolute',top: '0',left : '0',visibility : 'hidden'} );
						$('body').append($temp);
		                element.on('input', function () {
		                	var height = parseInt(element.css('height').replace('px',''));
		                	$('#temp-textarea').text( $(element).val() ).css( { 'height': min, 'overflow-y': 'hidden' } );
		                    element.css('height', $('#temp-textarea')[0].scrollHeight + 'px');
		                    if(height <= $('#temp-textarea')[0].scrollHeight){
		                		$('.container').scrollTop( $(element).height() );
		                	}		                 	
		                });
					}
				}
			}]) 
			.directive('materialInput', ()=> {
				return {
					restrict : 'E',
					replace : true,
					scope : { inModel : '=?' , inLabel : '@', inType : '@?', inTrim : '@?' },
					template : "<div class=\"material-input\"><input type=\"{{inType ? inType : 'text'}}\" required ng-model=\"inModel\" ng-trim=\"{{inTrim == undefined ? true : inTrim }}\"><span class=\"highlight\"></span><span class=\"bar\"></span><label>{{inLabel}}</label></div>"
				}
			})
			.directive('contenteditable', ['$sce', function($sce) {
			  return {
			    restrict: 'A', // only activate on element attribute
			    require: '?ngModel', // get a hold of NgModelController
			    link: function(scope, element, attrs, ngModel) {
			      if (!ngModel) return; // do nothing if no ng-model

			      // Specify how UI should be updated
			      ngModel.$render = function() {
			        element.html($sce.getTrustedHtml(ngModel.$viewValue || ''));
			      };

			      // Listen for change events to enable binding
			      element.on('blur keyup change', function() {
			        scope.$evalAsync(read);
			      });
			      read(); // initialize

			      // Write data to the model
			      function read() {
			        var html = element.html();
			        // When we clear the content editable the browser leaves a <br> behind
			        // If strip-br attribute is provided then we strip this out
			        if ( attrs.stripBr && html == '<br>' ) {
			          html = '';
			        }
			        ngModel.$setViewValue(html);
			      }
			    }
			  };
			}])
})();