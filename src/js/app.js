
import Info from '../info.json';
import Route from './routes';
import TabProduct from './directive/tab-product.js';
import TabOrder from './directive/tab-order.js';
import TabMember from './directive/tab-member.js';
import TabSetting from './directive/tab-setting.js';
import ModalTag from './directive/modal-tag.js';


import BSHeader from './directive/bs-header.js';
import BSNav from './directive/bs-nav.js';
import SessionService from './services/SessionService';
import FileService from './services/FileService';
import AlertService from './services/AlertService';
import 'bootstrap';
import 'animate.css';
import 'bootstrap/dist/css/bootstrap.css';
import '../css/nanumbarungothic.css';
import '../css/material-input.css';
import '../css/util.css';
import '../css/common.css';
import '../css/xeicon.css';
 

String.prototype.endsWith = function(str){
	if (this.length < str.length) { return false; }
	return this.lastIndexOf(str) + str.length == this.length;
}

var app = angular.module('bashow',['ui.router','ngAnimate','ngSanitize','rsea-angular'])
				.config(['$qProvider', ($qProvider)=> { $qProvider.errorOnUnhandledRejections(Info.mode != 'prodiction'); }])
				.config(Route)
				.service('$session', SessionService)
				.service('$fileService', FileService)
				.service('$as', AlertService)
				.directive('bsNav', BSNav)
				.directive('tabProduct', TabProduct)
				.directive('tabOrder', TabOrder)
				.directive('tabMember', TabMember)
				.directive('tabSetting', TabSetting)
				.directive('modalTag', ModalTag)
				.directive('bsHeader', BSHeader)
				.run(['$window','$rootScope','$state','$location','$animate',($window,$rootScope,$state,$location,$animate) => {
					$state.defaultErrorHandler( error=> {
						if(info != 'production') { 
							console.error(error)
						}
					});
					$rootScope.$on('$locationChangeStart', (event,newUrl)=> {
						$('.modal').modal('hide');
						$('ui-view').removeClass('back');
						newUrl.endsWith('lightbox') ? $('body').addClass('LightBox') : $('body').removeClass('LightBox');

					});	
					$rootScope.$on('$locationChangeSuccess', ()=> {
						$rootScope.actualLocation = $location.path();
					});	
				    $rootScope.$watch( ()=>$location.path() , (newLocation,oldLocation)=> {
						$('ui-view').addClass('back');
						oldLocation.endsWith('lightbox') ? $('body').addClass('LightBox') : $('body').removeClass('LightBox');
				    });
				}]);

