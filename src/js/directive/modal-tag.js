import Template from '../../templates/modal/tag.html';
export default function(){
	return {
		restrict : 'E',
		replace : true,
		scope : { modalValue : '=' },
		template : Template
	}	
}
