import Template from '../../templates/TabOrder.html';
export default function TabOrderDirective(){
	return { restrict : 'E' , replace: true , template : Template , controller : TabOrder }
}
class TabOrder {
	constructor($scope){
		$scope.orderTab = '1';
	}
}

TabOrder.$inject = ['$scope'];