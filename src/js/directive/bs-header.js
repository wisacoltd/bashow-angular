import Template from '../../templates/_header.html';
export default function(){
	return {
		restrict : 'E',
		replace : false,
		scope : { sharpOn : '=?' , sharpClick : '&?' , btnHidden : '@?'},
		template : Template,
		link : function(scope){
			console.log(scope.btnHidden);
			if(scope.btnHidden == undefined) scope.btnHidden = false;
		}
	}	
}
