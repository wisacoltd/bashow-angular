import Template from '../../templates/_nav.html';
export default function(){
	return {
		restrict : 'E',
		replace : true,
		scope : { navTitle : '=' , goBack : '&?', rightBtn : '@?', rightClick: '&?' },
		template : Template,
		link : function(scope){
			if(!scope.goBack){
				scope.goBack = ()=> window.history.back();
			}
		}
	}	
}
