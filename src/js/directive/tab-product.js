import Template from '../../templates/TabProduct.html';
export default function TabProductDirective(){
	return { restrict : 'E' , replace: true , template : Template , controller : TabProduct }
}
class TabProduct {
	constructor($scope,$injector){
		$scope.hl = this;
		this.$injector = $injector;
		this.$scope = $scope;
	}

	imgClick(){
		let $state = this.$injector.get('$state');
		$state.go('LightBox');
	}
}

TabProduct.$inject = ['$scope','$injector'];
