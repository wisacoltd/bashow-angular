import Template from '../../templates/TabMember.html';
export default function TabMemberDirective(){
	return { restrict : 'E' , replace: true , template : Template , controller : TabMember }
}
class TabMember {
	constructor($scope){
		$scope.memberTab = 'mbr';
	}
}
TabMember.$inject = ['$scope'];