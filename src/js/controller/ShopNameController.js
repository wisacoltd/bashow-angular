import BaseController from './$BaseController';
import Template from '../../templates/ShopName.html';

export const ShopNameTemplate = Template;
export function ShopNameResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class ShopName extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
		$scope.input = {
			name : {}
		};

		$scope.confirm = () => {
			$.each( $scope.input , (k,v)=>{ delete v.valid });
			$scope.input.name.valid = $scope.input.name.value ? 'success' : 'fail';
			if(!$scope.input.name.value){
				$scope.input.name.error = '쇼핑몰명을 입력해주세요.';
				return; 
			}
			// TODO: OK
			$window.history.back();
		}		
	}
}  
ShopName.$inject = ['$scope','$injector'];