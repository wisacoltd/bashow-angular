import BaseController from './$BaseController';
import Template from '../../templates/MemberOrder.html';

export const MemberOrderTemplate = Template;
export function MemberOrderResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class MemberOrder extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
	}
}  
MemberOrder.$inject = ['$scope','$injector'];