import BaseController from './$BaseController';
import Template from '../../templates/LightBox.html';
import Swiper from 'swiper';
export const LightBoxTemplate = Template;
export function LightBoxResolve(){ }

export class LightBox extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);	
		let self = this;
		$scope.$watch('$scope.imgs', ()=>{
			if(self.swiper) self.swiper.destroy();
			$scope.$applyAsync( ()=> {
				self.swiper = new Swiper('.swiper-container' , { navigation: { nextEl : '.swiper-button-next' , prevEl : '.swiper-button-prev'} } );
			})

		});
	}

	onCreate($scope,$injector){
		$scope.back = function(){
			window.history.back()
		}
		$scope.imgs = ['https://cdn.zeplin.io/5b4d8b1a93d9a2d8b8e7a925/assets/b72d6677-5dc8-44cd-a7d2-8067d7050178.png','https://cdn.zeplin.io/5b4d8b1a93d9a2d8b8e7a925/assets/b72d6677-5dc8-44cd-a7d2-8067d7050178.png'];
	}
}  
LightBox.$inject = ['$scope','$injector'];


 