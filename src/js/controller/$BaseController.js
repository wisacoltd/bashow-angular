export default class BaseController{
	constructor($scope,$injector){
		this.$scope = $scope;
		this.$injector = $injector;
		let self = this;
		let $location = $injector.get('$location');
		let $rootScope = $injector.get('$rootScope');
		let $state = $injector.get('$state');
		let path = $location.path();
		let controllerCache = $rootScope.actualLocation == path ? this.controllerCache($rootScope,path) : { restore : false};
		let isCache = $state.current.cache ? $state.current.cache : true;
		$scope.hl = this;
		if(controllerCache.restore){
			angular.extend(this,controllerCache.scope);
		}
		$scope.$on('$destroy' , ()=> {
	
			if(!isCache) return;
			if(!$rootScope.__controller) $rootScope.__controller = {};
			$rootScope.__controller[path] = { scope: {} , scroll : 0 };
			$rootScope.__controller[path].scroll = $('ui-view:nth-child(2) .container').scrollTop();
			$rootScope.__controller[path].restore = $rootScope.__controller[path].scroll  ? true : false;
			if($scope.hl){
				Object.keys($scope.hl).forEach( (k) => {
					if( k.indexOf('$') != 0){
						$rootScope.__controller[path].scope[k] = $scope.hl[k];
						$rootScope.__controller[path].restore = true;
					}
				})
			}
		})
		if(controllerCache.restore){
			this.onRestore($scope,$injector);
			var emit = $scope.$on('$viewContentLoaded', function() {
				if(!$rootScope.__controller) return;
				if(!$rootScope.__controller[path]) return;
				if($rootScope.__controller[path].scroll == 0 ) return;
				$scope.$applyAsync(function(){
					$('ui-view .container').scrollTop($rootScope.__controller[path].scroll);
				})
				emit();
	 		});
		}else{
			this.onCreate($scope,$injector);
		}
	}

	controllerCache($rootScope,path){
		if(!$rootScope.__controller) return {};
		if(!$rootScope.__controller[path]) return {};
		return $rootScope.__controller[path];
	}


	onCreate($scope,$injector) {}
	onRestore($scope,$injector){ }

}  