import BaseController from './$BaseController';
import Template from '../../templates/Reg.html';

export const RegTemplate = Template;
export function RegResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class Reg extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
		this.step = 1;
		this.input = {
			name : {},
			email : {},
			phone : {},
			password : {},
			shopId : {},
			companyNm : {}
		};
	}

	nextStep1(){
		$.each( this.input , (k,v)=>{ delete v.valid });
		this.input.name.valid = this.input.name.value ? 'success' : 'fail';
		if(!this.input.name.value){
			this.input.name.error = '이름을 입력하여주세요.';
			return;
		}
		this.input.email.valid = this.input.email.value ? 'success' : 'fail';
		if(!this.input.email.value){
			this.input.email.error = '이메일을 입력하여주세요.';
			return;
		}
		this.input.phone.valid = this.input.phone.value ? 'success' : 'fail';
		if(!this.input.phone.value){
			this.input.phone.error = '휴대폰번호를 입력하여주세요.';
			return;
		}
		this.input.password.valid = this.input.password.value ? 'success' : 'fail';
		if(!this.input.password.value){
			this.input.password.error = '비밀번호를 입력하여주세요.';
			return;
		}

		if(this.input.name.valid == 'success' 
			&& this.input.password.valid == 'success' 
			&& this.input.phone.valid == 'success' 
			&& this.input.password.valid == 'success'){
			// TODO : 가입여부 체크
			// TODO : GO NEXT STEP
			this.step = 2;
		}

	}

	allChange(){
		this.input.agree1 = this.input.agreeAll;
		this.input.agree2 = this.input.agreeAll;
		this.input.agree3 = this.input.agreeAll;
		this.input.agree4 = this.input.agreeAll;
		this.input.agree5 = this.input.agreeAll;
	}
	agreeChange(){
		this.input.agreeAll = (this.input.agree1 && this.input.agree2 && this.input.agree3 && this.input.agree4 && this.input.agree5);
	}		
	nextStep2(){
		this.input.shopId.valid = this.input.shopId.value ? 'success' : 'fail';
		if(!this.input.shopId.value){
			this.input.shopId.error = '쇼핑몰ID를 입력하여주세요.';
		}
		this.input.companyNm.valid = this.input.companyNm.value ? 'success' : 'fail';
		if(!this.input.companyNm.value){
			this.input.companyNm.error = '회사명을 입력하여주세요.';
		}
		if(this.input.agree1 && this.input.agree2 && this.input.agree3 && this.input.agree4 && this.input.agree5){
			//TODO : GO NEXT STEP
			this.step = 3;
		}
	}
	nextStep3(){
	 	this.$injector.get('$state').go('Auth',{},{ location: "replace" , reload: true });
	}
		
}  
Reg.$inject = ['$scope','$injector'];