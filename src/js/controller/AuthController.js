import BaseController from './$BaseController';
import Template from '../../templates/Auth.html';

export const AuthTemplate = Template;
export function AuthResolve(){
	return { }
}
export class Auth extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);

		$scope.input = {}
		$scope.loginClick = () => {
			if(!$scope.input.id){
				alert('아이디를 입력해주세요'); return;
			}
			if(!$scope.input.password){
				alert('비밀번호를 입력해주세요'); return;
			}

			// TODO : LOGIN API
			$injector.get('$session').save('TEMP_SESSION_KEY');
			$injector.get('$state').go('Main',{},{ location: "replace" , reload: true });
		}		
		
	}
}  
Auth.$inject = ['$scope','$injector'];


 