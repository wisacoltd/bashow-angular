import BaseController from './$BaseController';
import Template from '../../templates/Main.html';

export const MainTemplate = Template;
export function MainResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class Main extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
	}

	onCreate($scope,$injector){
		$scope.hl.tab = 'prd';
	}

}  
Main.$inject = ['$scope','$injector'];


 