import BaseController from './$BaseController';
import Template from '../../templates/Review.html';

export const ReviewTemplate = Template;
export function ReviewResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class Review extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
	}
}  
Review.$inject = ['$scope','$injector'];