import BaseController from './$BaseController';
import Template from '../../templates/Customer.html';

export const CustomerTemplate = Template;
export function CustomerResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class Customer extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
		$scope.$applyAsync( ()=> {
			$('.csm-det').removeClass('clear-transition');
		});
	}

	onCreate($scope,$injector){
		$scope.datas = [  {},{},{} ];
		$scope.rowClick = this.rowClick;
	}
	onRestore($scope,$injector){
	}

	rowClick($event,item) { 
		item.open=!item.open;
		var $det = $($event.target).parents('li').find('.csm-det');
		var height = 0;
		$det.children().each( (index,item)=> { 
			height = height + $(item).outerHeight(); 
		} )
		$det.css('height', height+'px');
		if(!item.open){
			$det.css('height', '0px');
		}
	}
}  
Customer.$inject = ['$scope','$injector'];



