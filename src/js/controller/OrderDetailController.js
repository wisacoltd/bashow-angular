import BaseController from './$BaseController';
import Template from '../../templates/OrderDetail.html';

export const OrderDetailTemplate = Template;
export function OrderDetailResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class OrderDetail extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
		$scope.orderCode = '20161223-770ed'.toUpperCase();
		$scope.statOpen = false;
		
	}
}  
OrderDetail.$inject = ['$scope','$injector'];