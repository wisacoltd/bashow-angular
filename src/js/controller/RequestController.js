import BaseController from './$BaseController';
import Template from '../../templates/Request.html';

export const RequestTemplate = Template;
export function RequestResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class Request extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
	}
}   
Request.$inject = ['$scope','$injector'];