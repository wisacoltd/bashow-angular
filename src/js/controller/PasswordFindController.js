import BaseController from './$BaseController';
import Template from '../../templates/PasswordFind.html';

export const PasswordFindTemplate = Template;
export function PasswordFindResolve(){
	return {  }
}
export class PasswordFind extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
		this.input = { phone : {} , number : {} , email : {} };
		this.sendedSMS = false;
		this.step = 1;
	}

	sendSMS(){
		$.each( this.input , (k,v)=>{ delete v.valid });

		this.input.phone.valid = this.input.phone.value ? 'success' : 'fail';
		if(!this.input.phone.value){
			this.input.phone.error = '전화번호를 입력하여주세요.';
		}		
		if(this.input.phone.valid=='success'){
			// TODO : 인증번호 발송
			this.sendedSMS = true;
		}
	}
	confirmClick(){
		$.each( this.input , (k,v)=>{ delete v.valid });
		if(this.input.findType == 'phone'){
			if(!this.sendedSMS){
				this.input.phone.valid = 'fail';
				this.input.phone.error = '휴대폰 인증이 되지않았습니다.';
				return;
			}
			this.input.number.valid = this.input.number.value ? 'success' : 'fail';
			if(!this.input.number.value){
				this.input.number.error = '인증번호를 입력하여주세요.';
				return;
			}
			this.step = 2;
		}else if(this.input.findType == 'email'){
			this.input.email.valid = this.input.email.value ? 'success' : 'fail';
			if(!this.input.email.value){
				this.input.email.error = '이메일을 입력하여주세요.';
				return;
			}
			// TODO: FIND RUN
			this.step = 2;	
		}		
	}
	loginClick(){
		this.$injector.get('$state').go('Auth',{},{ location: "replace" , reload: true });
	}
}  
PasswordFind.$inject = ['$scope','$injector'];