import BaseController from './$BaseController';
import Template from '../../templates/PhotoEditor.html';

export const PhotoEditorTemplate = Template;
export function PhotoEditorResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class PhotoEditor extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
		this.photo = [ 'http://m.sw.wisaweb.co.kr/_data/product/201804/03/9c68aa54fcb3ca1184597303d8744014.png', 
						'http://m.sw.wisaweb.co.kr/_data/product/201804/30/428a9ff47434db19115c7c0d320c6389.png#addimg', 
						'http://m.sw.wisaweb.co.kr/_data/product/201708/04/cac0bbac4d88c960d5ca8af826600a06.jpg#addimg' ]
	}

	onComplete(){
		window.history.back();
	}

}  
PhotoEditor.$inject = ['$scope','$injector'];