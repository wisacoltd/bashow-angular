import BaseController from './$BaseController';
import Template from '../../templates/Answer.html';

export const AnswerTemplate = Template;
export function AnswerResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class Answer extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
	}
}  
Answer.$inject = ['$scope','$injector'];


 