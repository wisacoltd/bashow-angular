import BaseController from './$BaseController';
import Template from '../../templates/MemberDetail.html';

export const MemberDetailTemplate = Template;
export function MemberDetailResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class MemberDetail extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);	
	}

	telClick(){
		this.$injector.get('$as').alert('강동원회원님에게<br>연결하시겠습니까?');
	}
}  
MemberDetail.$inject = ['$scope','$injector'];


 