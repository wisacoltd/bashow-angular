import BaseController from './$BaseController';
import Template from '../../templates/ProductReg.html';

export const ProductRegTemplate = Template;
export function ProductRegResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class ProductReg extends BaseController{
	constructor($scope,$injector,$fileService,$window){
		super($scope,$injector);
		var self = this;
		self.$file = $fileService;
		self.modal = { tags : [] , history : [ '핸드메이드', '슬렉스', '원피스', '첼시부츠','클러치백'] };
 
		self.modal.tagKeyDown = event=>{
			self.modal.before = $('.tag-input .field input').val();;
			$(event.target).width('40px');
			$(event.target).width( event.target.scrollWidth );
		}
		self.modal.tagKeyUp = event=>{
			// console.log($('.tag-input .field input[]').val());
			let value = $('.tag-input .field input').val();
			if(self.modal.before == undefined) return;
			console.log(self.modal.tag);
			if(self.modal.before.length == value.length && event.keyCode == 8 && value== self.modal.before && value  == ''){
				self.modal.tags.pop();
			}
		}
		self.modal.tagChange = event =>{
			console.log(self.modal.tag);
			if(self.modal.tag.length == 0) return;
			let last = self.modal.tag[self.modal.tag.length-1];
			if(last == ' '){
				let result = /([a-zA-Z0-9ㄱ-ㅎ가-힣])+/.exec(self.modal.tag);
				if(result){
					self.modal.tagProcess(result[0]);
					return;
				}
			}
		}
		self.modal.tagBlur = event=>{
			if(self.modal.tag){
				let result = /([a-zA-Z0-9ㄱ-ㅎ가-힣])+/.exec(self.modal.tag);
				if(result){
					self.modal.tagProcess(result[0]);
					return;
				}
			}
		}
		self.modal.tagProcess = (tag) =>{
			self.modal.tags.push(tag); 
			$('.tag-input li.field input').width('40px');
			self.modal.tag = '';
			self.modal.before = '';
		}
		self.modal.historyClick = item => { self.modal.tagProcess(item) }
	}

	

	onFileUpload(value){
		let self = this;
		let promise;
		if(value == 'camera'){
			promise = self.$file.cameraUpload('image');
		}else{
			promise = self.$file.albumUpload('image');
		}
		promise.then( () => {
			console.log($form);
		})
	}

	onTag(){
		$('#tag-modal').modal();
	}
	goPhotoEditor(){
		this.$injector.get('$state').go('PhotoEditor');
	}
	onComplete(){
		window.history.back();
	}

}  
ProductReg.$inject = ['$scope','$injector','$fileService','$window'];