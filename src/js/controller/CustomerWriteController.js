import BaseController from './$BaseController';
import Template from '../../templates/CustomerWrite.html';

export const CustomerWriteTemplate = Template;
export function CustomerWriteResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class CustomerWrite extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
	}
}  
CustomerWrite.$inject = ['$scope','$injector'];


 