import BaseController from './$BaseController';
import Template from '../../templates/NoticeSetting.html';

export const NoticeSettingTemplate = Template;
export function NoticeSettingResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class NoticeSetting extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
	}
}  
NoticeSetting.$inject = ['$scope','$injector'];