import BaseController from './$BaseController';
import Template from '../../templates/PasswordChange.html';

export const PasswordChangeTemplate = Template;
export function PasswordChangeResolve(){
	return { sessionKey : ($session) => { $session.check() } }
}
export class PasswordChange extends BaseController{
	constructor($scope,$injector){
		super($scope,$injector);
		$scope.input = {
			oldpwd : {},
			newpwd : {},
			newpwd_cfm : {}
		};

		$scope.confirm = () => {
			$.each( $scope.input , (k,v)=>{ delete v.valid });
			$scope.input.oldpwd.valid = $scope.input.oldpwd.value ? 'success' : 'fail';
			if(!$scope.input.oldpwd.value){
				$scope.input.oldpwd.error = '기존비밀번호를 입력해주세요.';
				return; 
			}
			$scope.input.newpwd.valid = $scope.input.newpwd.value ? 'success' : 'fail';
			if(!$scope.input.newpwd.value){
				$scope.input.newpwd.error = '신규비밀번호를 입력하여주세요.';
				return;
			}
			$scope.input.newpwd_cfm.valid = $scope.input.newpwd_cfm.value ? 'success' : 'fail';
			if(!$scope.input.newpwd_cfm.value){
				$scope.input.newpwd_cfm.error = '비밀번호 확인을 입력하여주세요.';
				return;
			}
			$scope.input.newpwd_cfm.valid = $scope.input.newpwd_cfm.value == $scope.input.newpwd.value ? 'success' : 'fail';
			if($scope.input.newpwd_cfm.value != $scope.input.newpwd.value){
				$scope.input.newpwd_cfm.error = '비밀번호가 일치하지 않습니다.';
				return;
			}
			// TODO: OK
			$window.history.back();
		}		
	}
}  
PasswordChange.$inject = ['$scope','$injector'];