import {Auth,AuthTemplate,AuthResolve} from './controller/AuthController';
import {Main,MainTemplate,MainResolve} from './controller/MainController';
import {MemberDetail,MemberDetailTemplate,MemberDetailResolve} from './controller/MemberDetailController';
import {MemberOrder,MemberOrderTemplate,MemberOrderResolve} from './controller/MemberOrderController';
import {OrderDetail,OrderDetailTemplate,OrderDetailResolve} from './controller/OrderDetailController';
import {PasswordFind,PasswordFindTemplate,PasswordFindResolve} from './controller/PasswordFindController';
import {PasswordChange,PasswordChangeTemplate,PasswordChangeResolve} from './controller/PasswordChangeController';
import {ProductReg,ProductRegTemplate,ProductRegResolve} from './controller/ProductRegController';
import {Request,RequestTemplate,RequestResolve} from './controller/RequestController';
import {Review,ReviewTemplate,ReviewResolve} from './controller/ReviewController';
import {Reg,RegTemplate,RegResolve} from './controller/RegController';
import {ShopName,ShopNameTemplate,ShopNameResolve} from './controller/ShopNameController';
import {Answer,AnswerTemplate,AnswerResolve} from './controller/AnswerController';
import {NoticeSetting,NoticeSettingTemplate,NoticeSettingResolve} from './controller/NoticeSettingController';
import {Customer,CustomerTemplate,CustomerResolve} from './controller/CustomerController';
import {CustomerWrite,CustomerWriteTemplate,CustomerWriteResolve} from './controller/CustomerWriteController';
import {LightBox,LightBoxTemplate,LightBoxResolve} from './controller/LightBoxController';
import {PhotoEditor,PhotoEditorTemplate,PhotoEditorResolve} from './controller/PhotoEditorController';

export default [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider', 
	($stateProvider,$urlRouterProvider,$locationProvider )=>{
		$stateProvider.state('Auth',{
			url : '/auth',
			template : AuthTemplate,
			resolve : AuthResolve,
			controller : Auth
		}); 		
		$stateProvider.state('Main',{
			url : '/main',
			template : MainTemplate,
			resolve : MainResolve,
			controller : Main
		}); 		
		$stateProvider.state('MemberDetail',{
			url : '/member/detail',
			template : MemberDetailTemplate,
			resolve : MemberDetailResolve,
			controller : MemberDetail
		}); 		
		$stateProvider.state('MemberOrder',{
			url : '/member/order',
			template : MemberOrderTemplate,
			resolve : MemberOrderResolve,
			controller : MemberOrder
		}); 		
		$stateProvider.state('OrderDetail',{
			url : '/order/detail',
			template : OrderDetailTemplate,
			resolve : OrderDetailResolve,
			controller : OrderDetail
		}); 		
		$stateProvider.state('PasswordFind',{
			url : '/password/find',
			template : PasswordFindTemplate,
			resolve : PasswordFindResolve,
			controller : PasswordFind
		}); 		
		$stateProvider.state('PasswordChange',{
			url : '/password/change',
			template : PasswordChangeTemplate,
			resolve : PasswordChangeResolve,
			controller : PasswordChange
		}); 		
		$stateProvider.state('ProductReg',{
			url : '/product/reg',
			template : ProductRegTemplate,
			resolve : ProductRegResolve,
			controller : ProductReg
		}); 		
		$stateProvider.state('Request',{
			url : '/request',
			template : RequestTemplate,
			resolve : RequestResolve,
			controller : Request
		}); 		
		$stateProvider.state('Review',{
			url : '/review',
			template : ReviewTemplate,
			resolve : ReviewResolve,
			controller : Review
		}); 		
		$stateProvider.state('Reg',{
			url : '/reg',
			template : RegTemplate,
			resolve : RegResolve,
			controller : Reg
		}); 		
		$stateProvider.state('ShopName',{
			url : '/shop/name',
			template : ShopNameTemplate,
			resolve : ShopNameResolve,
			controller : ShopName
		}); 		
		$stateProvider.state('Answer',{
			url : '/answer',
			template : AnswerTemplate,
			resolve : AnswerResolve,
			controller : Answer
		}); 		
		$stateProvider.state('NoticeSetting',{
			url : '/notice/setting',
			template : NoticeSettingTemplate,
			resolve : NoticeSettingResolve,
			controller : NoticeSetting
		}); 		
		$stateProvider.state('Customer',{
			url : '/customer',
			template : CustomerTemplate,
			resolve : CustomerResolve,
			controller : Customer
		}); 		
		$stateProvider.state('CustomerWrite',{
			url : '/customer/write',
			template : CustomerWriteTemplate,
			resolve : CustomerWriteResolve,
			controller : CustomerWrite
		}); 		
		$stateProvider.state('LightBox',{
			url : '/lightbox',
			template : LightBoxTemplate,
			resolve : LightBoxResolve,
			controller : LightBox,
			cache : false

		}); 	
		$stateProvider.state('PhotoEditor',{
			url : '/photo/editor',
			template : PhotoEditorTemplate,
			resolve : PhotoEditorResolve,
			controller : PhotoEditor,
			cache : false

		}); 			
		$urlRouterProvider.otherwise('/main');
 
	}
]; 

