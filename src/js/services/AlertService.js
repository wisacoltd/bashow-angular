import Template from '../../templates/modal/alert.html';
class AlertService {
	constructor($q){
		this.$q = $q;
	}
	alert(message){
		let deferred = this.$q.defer();
		let $modal = $(Template);
		$modal.find(".modal-body .message").html(message);
		$modal.find(".modal-footer .btn-primary").click( event=> deferred.resolve());
		$modal.modal();
		return deferred.promise;
	}
}
export default [
	'$q',
	AlertService
]