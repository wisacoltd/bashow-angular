class FileService {
	constructor($q,$state){
		this.$q = $q;
		this.$state = $state;
	}
	albumUpload(name){
		var deferred = this.$q.defer();
		$("form[id='temp-file']").remove();
		var $form = $(`<form id="temp-file"><input type="file" accept="image/*" name="${name}"></form>`);
		$form.find("input[type='file']").change( event => {
			if( $form.find("input[type='file']").val() == ''){
				deferred.reject();
			}else{
				//SERVER UPLOAD
				deferred.resolve();
			} 
		});
		$form.find("input[type='file']").trigger('click');
		return deferred.promise;		
	}
	cameraUpload(name){
		var deferred = this.$q.defer();
		$("form[id='temp-file']").remove();
		var $form = $(`<form id="temp-file"><input type="file" accept="image/*" name="${name}" capture="camera"></form>`);
		$form.find("input[type='file']").change( event => {
			if( $form.find("input[type='file']").val() == ''){
				deferred.reject();
			}else{
				//SERVER UPLOAD
				deferred.resolve();
			} 
		});
		$form.find("input[type='file']").trigger('click');
		return deferred.promise;		
	}
}
export default [
	'$q',
	'$state',
	FileService
]