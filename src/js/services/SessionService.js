class SessionService {
	constructor($q,$state){
		this.$q = $q;
		this.$state = $state;
	}
	get(){
		return window.localStorage.getItem('session');
	}
	save(sessionKey){
		window.localStorage.setItem('session',sessionKey);
	}
	check(){
		var deferred = $q.defer();
		var session = self.get();
		if(session){
			deferred.resolve(session);
		}else{
			deferred.reject('연결이 끊어졌습니다.');
			$state.go('Auth');
		}		
	}
}
export default [
	'$q',
	'$state',
	SessionService
]