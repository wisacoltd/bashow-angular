const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const fs = require('fs');


'use strict';

module.exports = (env,argv) =>{
	console.log(argv.mode);
	var json = { mode : argv.mode };
	if(argv.mode == 'development'){
		fs.writeFileSync('./src/info.json',JSON.stringify(json))
	}else if(argv.mode == 'production'){
		fs.writeFileSync('./src/info.json',JSON.stringify(json))
	}else{
		throw new Error('not support mode!'); 		
	}

	return {
		mode : argv.mode,
		entry : {
			vendor : ['jquery','angular','angular-animate','angular-sanitize','angular-resource','@uirouter/angularjs','swiper','./src/js/modules/rsea-angular.js','./src/js/libs/waves.js'],
			// vendor : ['jquery','./src/js/libs/ionic.bundle.min.js','./src/js/modules/rsea-angular.js','./src/js/libs/waves.js'],
			app : ['./src/js/app.js']
		},
		output : { 
			path : `${__dirname}/dist`,
			filename : '[name].min.js',
		},
		module : {
			rules : [
				{
					test: /\.js$/, 
					exclude: /node_modules/,
					use: { loader : 'babel-loader' ,  options: { presets : ['env'] }} 
				},
				{
					test: /\.json$/,
					use: { loader : 'json-loader'},
					type: 'javascript/auto'
				},
				{
					test: /\.css$/,
					use: ExtractTextPlugin.extract({
						fallback: 'style-loader',
						use : { loader : 'css-loader' , options: { minimize: true }}
					})
				},
				{
					test: /\.html$/,
					use: { loader : 'html-loader' , options : { minimize: true}}
				},
				{
					test: /\.(png|jpg|gif|svg|woff|ttf|eot|woff2)$/,
					use: { loader : 'file-loader'}
				},

			]
		},
		plugins : [
			new CleanWebpackPlugin(['dist']),
			new HtmlWebpackPlugin({
				template : `${__dirname}/src/index.html`,
				inject: 'true',
				filename : `${__dirname}/dist/index.html`,
				hash: true
			}),
			new ExtractTextPlugin( {  filename : 'app.css'}),
			new webpack.ProvidePlugin({
	  			$: 'jquery',
			  	jQuery : 'jquery',
		  		'window.jQuery': 'jquery',
			  	'window.$': 'jquery'
			})			
		],
		optimization: {
			minimizer : [
			 new UglifyJsPlugin({
		        uglifyOptions: {
		          beautify: false, compress: true, comments: true, mangle: false, toplevel: false, keep_classnames: true, keep_fnames: true 
		        }
		      })
			]
		},
		performance : { hints : false }
	}

}
